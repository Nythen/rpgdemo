﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Skill {

    public string name;
    public GameObject prefab;
    public int damage;
    public int cost;
    public int attackType;

    public Skill(string name, GameObject prefab, int damage, int cost, int attackType)
    {
        this.name = name;
        this.prefab = prefab;
        this.damage = damage;
        this.cost = cost;
        this.attackType = attackType;
    }
}
