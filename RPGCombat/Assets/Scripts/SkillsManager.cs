﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(TurnManager))]
public class SkillsManager : MonoBehaviour {

    public List<Skill> mageSkillsList = new List<Skill>();
    [SerializeField] private GameObject fireTornadoPrefab;
    private Skill fireTornadoSkill;
    
    public List<Skill> warriorSkillsList = new List<Skill>();
    private Skill warriorSmashSkill;

    public List<Skill> rogueSkillsList = new List<Skill>();
    private Skill rogueSlashSkill;

    [Header("UI")]
    [SerializeField] private GameObject skillPanel;
    [SerializeField] private GameObject enemyPanel;
    [SerializeField] private GameObject buttonPrefab;

    private int attackType;

    private TurnManager turnManager;

    void Start()
    {
        turnManager = GetComponent<TurnManager>();
        SettingSkills();
    }

    private void SettingSkills()
    {
        fireTornadoSkill = new Skill("Fire Tornado", fireTornadoPrefab, 27, 50, 5);
        mageSkillsList.Add(fireTornadoSkill);

        rogueSlashSkill = new Skill("Rogue Slash", null, 19, 15, 2);
        rogueSkillsList.Add(rogueSlashSkill);

        warriorSmashSkill = new Skill("Warrior Smash", null, 22, 15, 3);
        warriorSkillsList.Add(warriorSmashSkill);
    }

    public void AttackEnemy(GameObject enemy, Action onAttackFinished)
    {
        switch (attackType)
        {
            case 1:
                //Basic Attack
                ExecuteBasicAttack(enemy, onAttackFinished);
                break;
            case 2:
                //Rogue Slash
                ExecuteRogueSlash(enemy, onAttackFinished);
                break;
            case 3:
                //Warrior Smash
                ExecuteWarriorSmash(enemy, onAttackFinished);
                break;
            case 5:
                //Fire Tornado
                ExecuteFireTornado(enemy, onAttackFinished);
                break;
            default:
                //Default
                print("Default -> " + attackType);
                break;
        }
    }

    private void ExecuteBasicAttack (GameObject enemy, Action onAttackFinished)
    {
        print ("Basic Attack to " + enemy.name);
        TurnManager.getInstance().activeCharacterTurn.GetComponent<CharacterBattle>().SlideToTarget(enemy, () => {
            DoDamage(10f, enemy);
            onAttackFinished();
        });
    }

    private void ExecuteFireTornado(GameObject enemy, Action onAttackFinished)
    {
        GameObject currentFireTornado = Instantiate(fireTornadoSkill.prefab, enemy.transform.position, Quaternion.Euler(270f, 0f, 0));
        currentFireTornado.GetComponent<ParticleSystem>().Play();

        TurnManager.getInstance().activeCharacterTurn.GetComponent<CharacterBattle>().ThowSkillOrItem(5, ()=> {
            Destroy(currentFireTornado, 6);
            DoDamage(fireTornadoSkill.damage, enemy);
            DoCost(fireTornadoSkill.cost);
            onAttackFinished();
        });
    }

    private void ExecuteRogueSlash(GameObject enemy, Action onAttackFinished)
    {
        //GameObject currentSlash = Instantiate(rogueSlashSkill.prefab, enemy.transform.position, Quaternion.identity);
        //currentSlash.GetComponent<ParticleSystem>().Play();

        TurnManager.getInstance().activeCharacterTurn.GetComponent<CharacterBattle>().SlideToTarget(enemy, () => {
            //Destroy(currentSlash, 6f);
            DoDamage(rogueSlashSkill.damage, enemy);
            DoCost(rogueSlashSkill.cost);
            onAttackFinished();
        });
    }

    private void ExecuteWarriorSmash(GameObject enemy, Action onAttackFinished)
    {
        //GameObject currentSmash = Instantiate(warriorSmashSkill.prefab, enemy.transform.position, Quaternion.identity);
        //currentSmash.GetComponent<ParticleSystem>().Play();

        TurnManager.getInstance().activeCharacterTurn.GetComponent<CharacterBattle>().SlideToTarget(enemy, () => {
            //Destroy(currentSmash, 6f);
            DoDamage(warriorSmashSkill.damage, enemy);
            DoCost(warriorSmashSkill.cost);
            onAttackFinished();
        });
    }

    public void EnemyAttack(GameObject player, Action onAttackFinished)
    {
        print(player.name);
        print(TurnManager.getInstance().activeCharacterTurn.name);
        TurnManager.getInstance().activeCharacterTurn.GetComponent<CharacterBattle>().SlideToTarget(player, () => {
            DoDamage(20f, player);
            onAttackFinished();
        });
    }
    private void DoDamage(float amount, GameObject character)
    {
        character.GetComponent<CharStats>().TakeDamage(amount);
    }

    private void DoCost(float amount)
    {
        TurnManager.getInstance().activeCharacterTurn.GetComponent<CharStats>().CostToRest(amount);
    }

    //This method is called through the Skill Buttons (see BuildSkillButtons method)
    public void SetAttackType(int definedAttack)
    {
        attackType = definedAttack;
    }

    #region UI

    //Maybe better on another script?
    //This method is called on the Skill Button of the attackMenu
    public void ShowActiveCharSkills()
    {
        CharStats activeCharStats = TurnManager.getInstance().activeCharacterTurn.GetComponent<CharStats>();

        switch (activeCharStats.characterType)
        {
            case CharStats.CharacterType.Warrior:
                BuildSkillButtons(warriorSkillsList.ToArray());
                break;

            case CharStats.CharacterType.Mage:
                BuildSkillButtons(mageSkillsList.ToArray());
                break;

            case CharStats.CharacterType.Rogue:
                BuildSkillButtons(rogueSkillsList.ToArray());
                break;

            default:
                BuildSkillButtons(null);
                break;
        }
    }

    private void BuildSkillButtons(Skill[] listToBuild)
    {
        Button[] buttonsRefPosition = CombatManager.getInstance().attackButtons;

        //Clear skill buttons to replace it for the new ones
        foreach (Transform child in skillPanel.transform)
        {
            if (child.name != "BackButton")
                Destroy(child.gameObject);
        }

        for (int i = 0; i < listToBuild.Length; i++)
        {
            GameObject skillButton = Instantiate(buttonPrefab);

            var rectTransformReference = buttonsRefPosition[i].GetComponent<RectTransform>();

            var rectTransform = skillButton.GetComponent<RectTransform>();
            rectTransform.SetParent(skillPanel.transform);

            rectTransform.anchorMax = rectTransformReference.anchorMax;
            rectTransform.anchorMin = rectTransformReference.anchorMin;
            rectTransform.offsetMax = rectTransformReference.offsetMax;
            rectTransform.offsetMin = rectTransformReference.offsetMin;
            rectTransform.localScale = rectTransformReference.localScale;

            skillButton.SetActive(true);
            skillButton.GetComponentInChildren<Text>().text = listToBuild[i].name;

            int attackType = listToBuild[i].attackType;

            skillButton.GetComponent<Button>().onClick.AddListener(() => SetAttackType(attackType));
            skillButton.GetComponent<Button>().onClick.AddListener(() => enemyPanel.SetActive(true));
            skillButton.GetComponent<Button>().onClick.AddListener(() => skillPanel.SetActive(false));

            if (!HaveEnoughToCast(listToBuild[i]))
            {
                skillButton.GetComponent<Button>().interactable = false;
            }
        }
    }
    private bool HaveEnoughToCast(Skill skillToCast)
    {
        float currentCastBar = TurnManager.getInstance().activeCharacterTurn.GetComponent<CharStats>().getCastBarAmount();
        return currentCastBar >= skillToCast.cost;
    }

    #endregion
}