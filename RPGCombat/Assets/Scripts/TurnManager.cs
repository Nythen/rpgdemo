﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CombatManager))]
public class TurnManager : MonoBehaviour {

    static private TurnManager instance;
    public static TurnManager getInstance()
    {
        return instance;
    }

    [SerializeField] private GameObject turnIndicator;
    private GameObject prevTurnIndicator;

    private State state;
    private enum State
    {
        WaitingForPlayer,
        Busy,
    }

    public List<GameObject> character_BySpeed;

    public GameObject activeCharacterTurn;

    [Header("Turn bar - NOT WORKING, in progress")]
    [SerializeField] private List<Sprite> characterPics;
    [SerializeField] private Image turnBarBackground;
    [SerializeField] private Image charImage;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        BuildTurns();
    }

    public void BuildTurns()
    {
        GameObject[] players = CombatManager.getInstance().playersArray;
        GameObject[] enemies = CombatManager.getInstance().enemiesArray;

        GameObject[] allCharacters = players.Concat(enemies).ToArray();

        BuildCharList(allCharacters);
        character_BySpeed.Sort(SortBySpeed);

        //The first one is the faster
        activeCharacterTurn = character_BySpeed[0];
        StartCombat();

        //PrintImagesInSpeedOrder(); -> ToDo Need to be fixed
    }

    private void StartCombat()
    {
        InstantiateCharacterTurnIndicator();
        UpdateStateAndAttackMenu();
    }

    public void ChooseNextActiveCharacter()
    {
        CombatManager.getInstance().CheckBattleResults();

        if (!CombatManager.getInstance().isCombatFinished) 
        {
            int currentIndex = character_BySpeed.IndexOf(activeCharacterTurn);

            if (currentIndex == character_BySpeed.Count - 1)
            {
                activeCharacterTurn = character_BySpeed[0];
            }
            else
            {
                activeCharacterTurn = character_BySpeed[currentIndex + 1];
            }
            
            CharStats activeCharacterStats = activeCharacterTurn.GetComponent<CharStats>();

            if (activeCharacterTurn == null || activeCharacterStats.isDead == true)
            {
                ChooseNextActiveCharacter();
            }

            if (activeCharacterStats.isDefending)
            {
                print("active character was defending, canceling");
                activeCharacterStats.StopDefending();
            }

            InstantiateCharacterTurnIndicator();
            UpdateStateAndAttackMenu();
        }
    }

    private void InstantiateCharacterTurnIndicator()
    {
        Destroy(prevTurnIndicator);

        Vector3 characterPos = activeCharacterTurn.transform.position;
        Vector3 intantiatePos = new Vector3(characterPos.x, characterPos.y + 1.5f, characterPos.z);

        prevTurnIndicator = Instantiate(turnIndicator, intantiatePos, Quaternion.identity, activeCharacterTurn.transform);
    }

    public void UpdateStateAndAttackMenu()
    {
        GameObject[] players = CombatManager.getInstance().playersArray;
        Button[] attackButtons = CombatManager.getInstance().attackButtons;

        if (players.Contains(activeCharacterTurn))
        {
            foreach (Button button in attackButtons)
            {
                button.interactable = true;
            }
            state = State.WaitingForPlayer;
        }
        else
        {
            foreach (Button button in attackButtons)
            {
                button.interactable = false;
            }
            state = State.Busy;

            CombatManager.getInstance().AttackRandomPlayer();
        }
    }

    public void BlockAttackButtonsWhileAttacking()
    {
        foreach (Button button in CombatManager.getInstance().attackButtons)
        {
            button.interactable = false;
        }
        state = State.Busy;
    }

    private void BuildCharList(GameObject[] allCharacters)
    {        
        for (int i = 0; i < allCharacters.Length; i++)
        {
            character_BySpeed.Add(allCharacters[i]);
        }
    }
    
    public void PrintImagesInSpeedOrder()
    {
        float distanceBtwImages = turnBarBackground.rectTransform.sizeDelta.x / character_BySpeed.Count;
        print(turnBarBackground.rectTransform.sizeDelta.x + " / " + character_BySpeed.Count + " = " + distanceBtwImages);
        print(turnBarBackground.transform.position.x);

        for(int i = 0; i < character_BySpeed.Count; i++)
        {
            for (int j = 0; j < characterPics.Count; j++) 
            {
                if (character_BySpeed[i].name.Contains(characterPics[j].name))
                {
                    charImage.sprite = characterPics[j];
                    Vector3 intantiatePosition = new Vector3(turnBarBackground.transform.position.x + (i * distanceBtwImages), turnBarBackground.transform.position.y, 0);
                    print(intantiatePosition);
                    Instantiate(charImage, intantiatePosition, Quaternion.identity, turnBarBackground.transform);
                    print(characterPics[j].name + " Intantiated");
                }
            }
        }
    }
    private int SortBySpeed(GameObject char1, GameObject char2)
    {
        float speed1 = char1.GetComponent<CharStats>().speed;
        float speed2 = char2.GetComponent<CharStats>().speed;

        return speed2.CompareTo(speed1);
    }
}
