﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemsHandler : MonoBehaviour {

    private GameObject selectedCharacter;
    private Item selectedItem;

    public GameObject itemEffect;
    public List<Item> itemList = new List<Item>();
    private Item healPotion;
    private Item manaPotion;
    private Item rageBost;
    private Item energyPotion;

    [SerializeField] private GameObject itemPanel;
    [SerializeField] private GameObject allyPanel;
    [SerializeField] private GameObject buttonPrefab;

    private TurnManager turnManager;

    // Use this for initialization
    void Start () {
        turnManager = GetComponent<TurnManager>();
        //Creating items
        SettingItems();
	}

    private void SettingItems()
    {
        healPotion = new Item("Healing Potion", 25, 2);
        itemList.Add(healPotion);

        manaPotion = new Item("Mana Potion", 50, 2);
        itemList.Add(manaPotion);

        rageBost = new Item("Rage Booster", 50, 1);
        itemList.Add(rageBost);

        energyPotion = new Item("Energy Potion", 20, 2);
        itemList.Add(energyPotion);
    }

    public void ThrowPotion()
    {
        CharStats charStats = selectedCharacter.GetComponent<CharStats>();
        float amount = selectedItem.amountToRestore;

        ParticleSystem.MainModule ma = itemEffect.GetComponent<ParticleSystem>().main;

        //I'll change the action and the color of the effect depending on the item type
        switch (selectedItem.name)
        {
            case "Healing Potion":
                charStats.Heal(amount);
                ma.startColor = new Color(0.4f, 1, 0.2f);
                break;
            case "Mana Potion":
                ma.startColor = new Color(0f, 0.45f, 0.8f);
                charStats.RestoreCastBar(amount);
                break;
            case "Rage Booster":
                ma.startColor = new Color(0.85f, 0, 0);
                charStats.RestoreCastBar(amount);
                break;
            case "Energy Potion":
                ma.startColor = new Color(0.9f, 0.7f, 0);
                charStats.RestoreCastBar(amount);
                break;
            default:
                Debug.LogError("The item doesn't exist");
                break;
        }

        turnManager.BlockAttackButtonsWhileAttacking();

        GameObject currentItemEffect = Instantiate(itemEffect, selectedCharacter.transform.position, Quaternion.Euler(270f, 0f, 0));
        currentItemEffect.GetComponent<ParticleSystem>().Play();

        turnManager.activeCharacterTurn.GetComponent<CharacterBattle>().ThowSkillOrItem(2, () => {
            Destroy(currentItemEffect);

            //Update the used item quantity
            int i = itemList.IndexOf(selectedItem);
            itemList[i] = new Item(selectedItem.name, selectedItem.amountToRestore, selectedItem.qty - 1);

            turnManager.ChooseNextActiveCharacter();
        });
    }

    #region UI

    //Called on the Item Button
    public void ShowAvaibleItems()
    {
        BuildItemMenu(itemList.ToArray());
    }

    private void BuildItemMenu(Item[] listToBuild)
    {
        Button[] buttonsRefPosition = CombatManager.getInstance().attackButtons;

        //Clear item buttons to replace it for the new ones
        foreach (Transform child in itemPanel.transform)
        {
            if (child.name != "BackButton")
                Destroy(child.gameObject);
        }

        for (int i = 0; i < listToBuild.Length; i++)
        {
            GameObject itemButton = Instantiate(buttonPrefab);

            var rectTransformReference = buttonsRefPosition[i].GetComponent<RectTransform>();

            var rectTransform = itemButton.GetComponent<RectTransform>();
            rectTransform.SetParent(itemPanel.transform);

            rectTransform.anchorMax = rectTransformReference.anchorMax;
            rectTransform.anchorMin = rectTransformReference.anchorMin;
            rectTransform.offsetMax = rectTransformReference.offsetMax;
            rectTransform.offsetMin = rectTransformReference.offsetMin;
            rectTransform.localScale = rectTransformReference.localScale;

            itemButton.SetActive(true);
            Item item = listToBuild[i];

            itemButton.GetComponentInChildren<Text>().text = item.name + " x" + item.qty;

            itemButton.GetComponent<Button>().onClick.AddListener(() => SetSelectedItem(item));
            itemButton.GetComponent<Button>().onClick.AddListener(() => allyPanel.SetActive(true));
            itemButton.GetComponent<Button>().onClick.AddListener(() => itemPanel.SetActive(false));

            if (listToBuild[i].qty <= 0)
            {
                itemButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    //Method called through each Item buttons
    public void SetSelectedItem(Item itemToThrow)
    {
        selectedItem = itemToThrow;
        BuildAllyMenu();
    }

    private void BuildAllyMenu()
    {
        GameObject[] players = CombatManager.getInstance().playersArray;
        Button[] buttonsRefPosition = CombatManager.getInstance().attackButtons;

        for (int i = 0; i < players.Length; i++)
        {
            GameObject allyButton = Instantiate(buttonPrefab);

            var rectTransformReference = buttonsRefPosition[i].GetComponent<RectTransform>();

            var rectTransform = allyButton.GetComponent<RectTransform>();
            rectTransform.SetParent(allyPanel.transform);

            rectTransform.anchorMax = rectTransformReference.anchorMax;
            rectTransform.anchorMin = rectTransformReference.anchorMin;
            rectTransform.offsetMax = rectTransformReference.offsetMax;
            rectTransform.offsetMin = rectTransformReference.offsetMin;
            rectTransform.localScale = rectTransformReference.localScale;

            allyButton.SetActive(true);
            GameObject player = players[i];

            allyButton.GetComponentInChildren<Text>().text = player.name;

            allyButton.GetComponent<Button>().onClick.AddListener(() => SetSelectedCharacter(player));
            allyButton.GetComponent<Button>().onClick.AddListener(() => ThrowPotion());
            allyButton.GetComponent<Button>().onClick.AddListener(() => allyPanel.SetActive(false));

            if (!PlayerIsSelectable(player))
            {
                allyButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    private bool PlayerIsSelectable(GameObject player)
    {
        CharStats charStats = player.GetComponent<CharStats>();

        if (charStats.isDead)
        {
            return false;
        }
        else
        {
            //Only selectable on the items the ally could use
            string itemName = selectedItem.name;

            switch (charStats.characterType)
            {
                case CharStats.CharacterType.Warrior:
                    if (itemName.Equals(healPotion.name) || itemName.Equals(rageBost.name))
                        return true;
                    else
                        return false;

                case CharStats.CharacterType.Mage:
                    if (itemName.Equals(healPotion.name) || itemName.Equals(manaPotion.name))
                        return true;
                    else
                        return false;

                case CharStats.CharacterType.Rogue:
                    if (itemName.Equals(healPotion.name) || itemName.Equals(energyPotion.name))
                        return true;
                    else
                        return false;

                default:
                    return false;
            }
        }
    }

    //Method called through each Ally buttons
    public void SetSelectedCharacter(GameObject charToThrowItem)
    {
        selectedCharacter = charToThrowItem;
    }

    #endregion
}
