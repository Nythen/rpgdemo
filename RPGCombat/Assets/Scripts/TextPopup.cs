﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextPopup : MonoBehaviour {

    private const float DISAPPEAR_TIMER_MAX = 1f;

    private TextMeshPro textMesh;
    private float disappearTimmer;
    private Color textColor;

    private void Awake()
    {
        textMesh = transform.GetComponent<TextMeshPro>();
    }
    public void Setup(float amount, Color fontColor)
    {
        textMesh.SetText(amount.ToString());
        textColor = fontColor;
        textMesh.color = textColor;
        disappearTimmer = DISAPPEAR_TIMER_MAX;
    }

    private void Update()
    {
        float moveYspeed = 1f;
        transform.position += new Vector3(0, moveYspeed) * Time.deltaTime;

        /*if (disappearTimmer > DISAPPEAR_TIMER_MAX * 0.5f)
        {
            //First half of popup lifetime
            float increaseScaleAmount = 1f;
            transform.localScale += Vector3.one * increaseScaleAmount * Time.deltaTime;
        }
        else
        {
            //Second half
            float decreaseScaleAmount = 1f;
            transform.localScale -= Vector3.one * decreaseScaleAmount * Time.deltaTime;
        }*/

        disappearTimmer -= Time.deltaTime;
        if (disappearTimmer < 0)
        {
            //Reduce alpha to make the text disappear
            float disappearSpeed = 10f;
            textColor.a -= disappearSpeed * Time.deltaTime;
            textMesh.color = textColor;
            if (textColor.a < 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
