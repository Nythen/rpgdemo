﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SkillsManager))]
[RequireComponent(typeof(TurnManager))]
public class CombatManager : MonoBehaviour
{
    static private CombatManager instance;
    public static CombatManager getInstance()
    {
        return instance;
    }

    [SerializeField] private GameObject buttonPrefab;

    [Header("Enemies")]
    public GameObject[] enemiesArray;
    [SerializeField] private GameObject enemyPanel;
    [HideInInspector] public GameObject selectedEnemy;

    [Header("Characters")]
    public GameObject[] playersArray;

    [Header("AttackMenu")]
    [SerializeField] private GameObject attackPanel;
    public Button[] attackButtons;

    private SkillsManager skillsManager;
    private TurnManager turnManager;
    private ResultMenu resultMenu;

    [HideInInspector] public bool isCombatFinished = false;

    void Awake()
    {
        instance = this;

        BuildCharacterArrays();
    }

    void Start()
    {
        skillsManager = GetComponent<SkillsManager>();
        turnManager = GetComponent<TurnManager>();
        resultMenu = FindObjectOfType<ResultMenu>();
        attackButtons = attackPanel.GetComponentsInChildren<Button>();
        
        BuildSelectEnemyMenu();
    }

    private void BuildCharacterArrays()
    {
        playersArray = GameObject.FindGameObjectsWithTag("Player");
        enemiesArray = GameObject.FindGameObjectsWithTag("Enemy");
    }

    public void CheckBattleResults()
    {
        int deathCounts = 0;

        for (int i = 0; i < playersArray.Length; i++)
        {
            if (playersArray[i].GetComponent<CharStats>().isDead)
            {
                deathCounts++;
            }
        }

        if(deathCounts == playersArray.Length)
        {
            //you lose
            isCombatFinished = true;
            resultMenu.WinOrLoseResultMenu(false);
            print("lose");
        }

        if (enemiesArray.Length == 0)
        {
            //you win
            isCombatFinished = true;
            resultMenu.WinOrLoseResultMenu(true);
            print("win");
        }
    }

    public void DeleteEnemyFromList(GameObject enemyToDelete)
    {
        List<GameObject> enemiesList = new List<GameObject>(enemiesArray);

        enemiesList.Remove(enemyToDelete);
        enemiesArray = enemiesList.ToArray();

        if (enemiesArray.Length > 0)
        {
            BuildSelectEnemyMenu();
        }     
    }

    //This method only work for a maximum of 4 enemies, in case you need more remember to upgrade it
    public void BuildSelectEnemyMenu()
    {
        //Clear enemy buttons to replace it for the new ones
        foreach (Transform child in enemyPanel.transform)
        {
            if (child.name != "BackButton")
                Destroy(child.gameObject);
        }

        enemyPanel.SetActive(true);

        for (int i = 0; i < enemiesArray.Length; i++)
        {
            GameObject button = Instantiate(buttonPrefab);

            var rectTransformEnemy = button.GetComponent<RectTransform>();
            rectTransformEnemy.SetParent(enemyPanel.transform);

            var rectTransformAttack = attackButtons[i].GetComponent<RectTransform>();

            rectTransformEnemy.anchorMax = rectTransformAttack.anchorMax;
            rectTransformEnemy.anchorMin = rectTransformAttack.anchorMin;
            rectTransformEnemy.offsetMax = rectTransformAttack.offsetMax;
            rectTransformEnemy.offsetMin = rectTransformAttack.offsetMin;
            rectTransformEnemy.localScale = rectTransformAttack.localScale;

            button.SetActive(true);

            GameObject enemy = enemiesArray[i];

            button.GetComponentInChildren<Text>().text = enemy.name;

            button.GetComponent<Button>().onClick.AddListener(() => AttackSelectedEnemy(enemy));
            button.GetComponent<Button>().onClick.AddListener(() => enemyPanel.SetActive(false));
            enemyPanel.SetActive(false);
        }
    }

    public void AttackSelectedEnemy(GameObject enemy)
    {
        turnManager.BlockAttackButtonsWhileAttacking();
        selectedEnemy = enemy;
        skillsManager.AttackEnemy(enemy, () => {
            turnManager.ChooseNextActiveCharacter();
        });
        
    }

    public void AttackRandomPlayer()
    {
        int randomNumber = Random.Range(0, playersArray.Length); ;
        //This will never be a bucle because the code doesn't get here if the combat is over
        if (playersArray[randomNumber].GetComponent<CharStats>().isDead)
        {
            AttackRandomPlayer();
        }
        else
        {
            skillsManager.EnemyAttack(playersArray[randomNumber], () => {
                turnManager.ChooseNextActiveCharacter();
            });
        }
    }

    //Called though Defense button
    public void Defense()
    {
        turnManager.activeCharacterTurn.GetComponent<CharStats>().Defending();
        turnManager.ChooseNextActiveCharacter();
    }


}