﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultMenu : MonoBehaviour {

    public GameObject resultMenuPanel;
    public Text resultText;

	// Use this for initialization
	void Start () {
        resultMenuPanel.SetActive(false);
	}

    public void WinOrLoseResultMenu (bool isWin)
    {
        resultMenuPanel.SetActive(true);

        if (isWin)
        {
            resultText.text = "You win!!";
            resultText.color = Color.green;
        }
        else
        {
            resultText.text = "You lose.";
            resultText.color = Color.red;
        }
    }

    public void RetryButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitButton()
    {
        Application.Quit();
    }	
}
