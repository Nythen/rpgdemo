﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterBattle : MonoBehaviour {

    private State state;
     enum State
    {
        Idle,
        BasicAttack,
        Skill,
    }

    private Vector3 basicAttackPosition;
    private Action onBasicAttackComplete;

    private float secondsToWait;
    private Action onActionComplete;
    private float timer = 0.0f;

    void Update()
    {
        switch (state)
        {
            case State.Idle:
                break;
            case State.BasicAttack:
                transform.position += (basicAttackPosition - GetPosition()) * 3f * Time.deltaTime;

                float reachedDistance = 1f;
                if (Vector3.Distance(GetPosition(), basicAttackPosition) < reachedDistance)
                {
                    transform.position = basicAttackPosition;
                    onBasicAttackComplete();
                }
                break;
            case State.Skill:
                timer += Time.deltaTime;
                int seconds = Convert.ToInt32(timer % 60);
                if (seconds > secondsToWait)
                {
                    onActionComplete();
                }

                break;
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void SlideToTarget(GameObject target, Action onAttackFinished)
    {
        Vector3 targetPos = target.transform.position;
        Vector3 startingPosition = GetPosition();

        SlideToPosition((targetPos + GetPosition()) / 2, () => {
            //Arrived to target (Anim to attack Target)

            //Attack completed, slide back
            SlideToPosition(startingPosition, () =>
            {
                state = State.Idle;
                onAttackFinished();
            });
        });
    }

    private void SlideToPosition(Vector3 basicAttackPosition, Action onBasicAttackComplete)
    {
        this.basicAttackPosition = basicAttackPosition;
        this.onBasicAttackComplete = onBasicAttackComplete;
        state = State.BasicAttack;
    }

    public void ThowSkillOrItem(float secondsToWait, Action onActionComplete)
    {
        WaitTillThrowing(secondsToWait, () => {
            //Finished awesome skill of item effect
            state = State.Idle;
            timer = 0.0f;
            onActionComplete();
        });
    }

    private void WaitTillThrowing(float secondsToWait, Action onActionComplete)
    {
        this.secondsToWait = secondsToWait;
        this.onActionComplete = onActionComplete;
        state = State.Skill;
    }
}
