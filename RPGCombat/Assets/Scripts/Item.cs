﻿
public struct Item {

    public string name;
    public float amountToRestore;
    public int qty;

    public Item(string name, float amountToRestore, int qty)
    {
        this.name = name;
        this.amountToRestore = amountToRestore;
        this.qty = qty;
    }

    public void ChangeQuantity(int newQty)
    {
        this.qty = newQty;
    }
}
