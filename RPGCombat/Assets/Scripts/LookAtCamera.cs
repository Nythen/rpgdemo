﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {

    private Transform mainCamera;

    void Start()
    {
        //Camera needs to have the "MainCamera" tag
        mainCamera = Camera.main.transform;
        transform.LookAt(mainCamera);
    }
}
