﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayNight : MonoBehaviour {

    [SerializeField] private Light sun;
    [Range(0.5f, 2f)] public float delay = 1f;
    public bool startOnDay = true;
    [SerializeField] private Toggle dayNightUI;

    [Header("Night")]
    public Color nightSunColor = new Color (20,4,5);
    public Color nightAmbientColor = new Color(4,3,3);
    public Color nightfogColor = new Color(43,29,51,132);
    [SerializeField] private Material skyboxNight;

    [Header("Day")]
    public Color daySunColor = new Color(253, 250, 228);
    public Color dayAmbientColor = new Color(95,109,109);
    public Color dayfogColor = new Color(108,129,140,132);
    [SerializeField] private Material skyboxDay;

    private IEnumerator DayNightCoroutine = null;

    private void Start(){
        if (startOnDay)
        {
            dayNightUI.isOn = true;
            sun.color = daySunColor;
            RenderSettings.ambientSkyColor = dayAmbientColor;
            RenderSettings.fogColor = dayfogColor;
            RenderSettings.skybox = skyboxDay;
        }
        else
        {
            dayNightUI.isOn = false;
            sun.color = nightSunColor;
            RenderSettings.ambientSkyColor = nightAmbientColor;
            RenderSettings.fogColor = nightfogColor;
            RenderSettings.skybox = skyboxNight;
        }
    }

    public void ChangeDayNight(bool isDay)
    {
        if (DayNightCoroutine != null) { 
            StopCoroutine(DayNightCoroutine);
            DayNightCoroutine = null;
        }

        DayNightCoroutine = ChangingDayNight(isDay);
        StartCoroutine(DayNightCoroutine);
    }

    private IEnumerator ChangingDayNight(bool isDay)
    {
        float t = 0;
        if (isDay)
        {
            while (t < 1)
            {
                t += Time.deltaTime / delay;
                sun.color = Color.Lerp(nightSunColor, daySunColor, t);

                RenderSettings.ambientSkyColor = Color.Lerp(nightAmbientColor, dayAmbientColor, t);
                RenderSettings.fogColor = Color.Lerp(nightfogColor, dayfogColor, t);
                RenderSettings.skybox = skyboxDay;
                yield return null;
            }
        }
        else
        {
            while (t < 1)
            {
                t += Time.deltaTime / delay;
                sun.color = Color.Lerp(daySunColor, nightSunColor, t);

                RenderSettings.ambientSkyColor = Color.Lerp(dayAmbientColor, nightAmbientColor, t);
                RenderSettings.fogColor = Color.Lerp(dayfogColor, nightfogColor, t);
                RenderSettings.skybox = skyboxNight; 
                 yield return null;
            }
        }
    }

}
