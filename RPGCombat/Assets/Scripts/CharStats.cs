﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharStats : MonoBehaviour {

    public enum CharacterType {Warrior, Mage, Rogue, Enemy};
    public CharacterType characterType;

    [Header("Warrior Stats")]
    public float maxRage = 100;
    public float rage;
    public Slider rageBar;

    [Header("Mage Stats")]
    public float maxMana = 100;
    public float mana;
    public Slider ManaBar;

    [Header("Rogue Stats")]
    public float maxEnergy = 100;
    public float energy;
    public Slider energyBar;

    [Header("Common Stats")]
    public Slider healthBar;
    public float maxHealth = 100;
    private float health;
    [Range (0,10)] public int defense;
    [HideInInspector] public bool isDefending;
    public float speed = 5;
    public bool isDead = false;

    [Header("UI Elements")]
    [SerializeField] private Text healthNumber;
    [SerializeField] private Text costNumber;

    [SerializeField] private Transform pfTextPopup;

	void Start () {
        health = maxHealth;

        if (healthNumber != null) {
            healthNumber.text = health + " / " + maxHealth; 
        }

        switch (characterType)
        {
            case CharacterType.Mage:
                mana = maxMana;
                costNumber.text = mana + " / " + maxMana;
                break;
            case CharacterType.Warrior:
                rage = maxRage;
                costNumber.text = rage + " / " + maxRage;
                break;
            case CharacterType.Rogue:
                energy = maxEnergy;
                costNumber.text = energy + " / " + maxEnergy;
                break;
            default:
                break;
        }
    }

    public void CostToRest(float amount)
    {
        switch (characterType)
        {
            case CharacterType.Mage:
                mana -= amount;
                ManaBar.value = mana / maxMana;
                costNumber.text = mana + " / " + maxMana;
                break;
            case CharacterType.Warrior:
                rage -= amount;
                rageBar.value = rage / maxRage;
                costNumber.text = rage + " / " + maxRage;
                break;
            case CharacterType.Rogue:
                energy -= amount;
                energyBar.value = energy / maxEnergy;
                costNumber.text = energy + " / " + maxEnergy;
                break;
            default:
                break;
        }
    }

    public float getCastBarAmount()
    {
        switch (characterType)
        {
            case CharacterType.Mage:
                return mana;
            case CharacterType.Warrior:
                return rage;
            case CharacterType.Rogue:
                return energy;
            default:
                return 0;
        }
    }

    public void Heal(float amount)
    {
        if (health + amount >= maxHealth)
        {
            health = maxHealth;
        }
        else
        {
            health += amount;
        }

        if (healthNumber != null)
        {
            healthNumber.text = health + " / " + maxHealth;
        }

        healthBar.value = health / maxHealth;
        CreateTextPopup(amount, Color.green);
    }

    public void RestoreCastBar(float amount)
    {
        switch (characterType)
        {
            case CharacterType.Mage:
                if (mana + amount >= maxMana)
                {
                    mana = maxMana;
                }
                else
                {
                    mana += amount;
                }
                ManaBar.value = mana / maxMana;
                costNumber.text = mana + " / " + maxMana;
                CreateTextPopup(amount, Color.blue);
                break;
            case CharacterType.Warrior:
                if (rage + amount >= maxRage)
                {
                    rage = maxRage;
                }
                else
                {
                    rage += amount;
                }
                rageBar.value = rage / maxRage;
                costNumber.text = rage + " / " + maxRage;
                CreateTextPopup(amount, Color.red);
                break;
            case CharacterType.Rogue:
                if (energy + amount >= maxEnergy)
                {
                    energy = maxEnergy;
                }
                else
                {
                    energy += amount;
                }
                energyBar.value = energy / maxEnergy;
                costNumber.text = energy + " / " + maxEnergy;
                CreateTextPopup(amount, Color.yellow);
                break;
            default:
                break;
        }
    }

    public void TakeDamage(float amount)
    {
        amount = CalculateDamageWithDefense(amount);
        health -= amount;

        healthBar.value = health / maxHealth;

        CreateTextPopup(amount, Color.red);

        if (healthNumber != null)
        {
            healthNumber.text = health + " / " + maxHealth;
        }

        if (health <= 0)
        {
            Die();
        }
    }

    private float CalculateDamageWithDefense(float amount)
    {
        float damage = amount;
        
        if (defense != 0)
        {
            damage -= amount * ((float)defense / 10);
            damage = Mathf.Round(damage);
        }
        
        return damage;
    }

    public void Defending()
    {
        isDefending = true;
        defense += 3;
        print("Defending " + defense);
    }

    public void StopDefending()
    {
        isDefending = false;
        defense -= 3;
        print("StopDefending " + defense);
    }

    private void Die()
    {
        print(gameObject.name + " IS DED");

        if (gameObject.tag == "Enemy")
        {
            CombatManager.getInstance().DeleteEnemyFromList(gameObject);
            Destroy(gameObject);
        }
        
        isDead = true;
    }

    private void CreateTextPopup(float amount, Color fontColor)
    {
        Vector3 posToInstatiate = new Vector3(transform.position.x, transform.position.y+1, transform.position.z);
        Transform textPopupTransform = Instantiate(pfTextPopup, posToInstatiate, Quaternion.identity);
        textPopupTransform.LookAt(Camera.main.transform);

        TextPopup textPopup = textPopupTransform.GetComponent<TextPopup>();
        textPopup.Setup(amount, fontColor);
    }
}
